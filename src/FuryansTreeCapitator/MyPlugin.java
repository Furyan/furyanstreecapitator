package FuryansTreeCapitator;

import java.io.File;
import java.util.ArrayList;

import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_Block;
import PluginReference.MC_BlockType;
import PluginReference.MC_ItemStack;
import PluginReference.MC_ItemType;
import PluginReference.MC_Location;
import PluginReference.MC_Player;
import PluginReference.MC_Server;
import PluginReference.MC_World;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

import com.furyan.util.Config;
import com.furyan.util.StartMetrics;
import com.furyan.util.TreeCapitatorCommand;

/*
 * Health - Applied on login*, closing inventory*, respawn*, targeting (for mobs).
 * Damage, life steal, attack speed, critical chance, critical damage - Applied on attack.
 * Regen - Applied when player would normally regenerate health.
 * Item Restriction - Checked on inventory close, shooting bow / attacking, and crafting
 * Dodge, Armor - Applied on taking damage from another player or mob
 * Level Required - Checked on inventory close, shooting bow / attacking, and crafting
 */
public class MyPlugin extends PluginBase
{
	public static int MAJOR_VERSION = 0;
	public static int MINOR_VERSION = 2;
	public static String VERSION_TAG = "ALPHA";
	public static String PLUGIN_NAME = "FuryansTreeCapitator";
	public static String PLUGIN_TITLE = "Furyan's Tree Capitator";
	public Config config = null;
	public File configFile = null;
	public MC_Server Server = null;
	private MC_Player player;

	public PluginInfo getPluginInfo()
	{
		PluginInfo info = new PluginInfo();
		info.description = PLUGIN_TITLE + " v" + MAJOR_VERSION + "." + MINOR_VERSION + VERSION_TAG + "";
		info.name = PLUGIN_NAME;
		info.version = MAJOR_VERSION + "." + MINOR_VERSION + VERSION_TAG;

		return info;
	}

	public void onTick(int tickNumber)
	{
	}

	public void onStartup(MC_Server server)
	{
		Pl3xLibs.getScheduler().scheduleTask(PLUGIN_NAME, new StartMetrics(this.getPluginInfo()), 100);
		System.out.println("=-=-= " + PLUGIN_TITLE + " is starting up!");
		Server = server;

		System.out.println("=-=-= * Loading Configuration...");

		config = new Config();
		config.LoadConfig();

		Server.registerCommand(new TreeCapitatorCommand());
	}

	public static File getPluginConfigDirectory()
	{
		File jar = new File(MyPlugin.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		File dir = new File(jar.getParent() + File.separator + PLUGIN_NAME + File.separator);
		if (!dir.exists())
		{
			dir.mkdirs();
		}

		return dir;
	}

	public void onShutdown()
	{
		System.out.println("=-=-= " + PLUGIN_NAME + " is shutting down!");
		config.SaveConfig();

	}

	public void DisplayDebugMessage(String message)
	{
		if (config.DisplayDebugMessages)
		{
			System.out.println("=-=DEBUG=-= " + message);
		}
	}

	public void onBlockBroke(MC_Player player, MC_Location loc, MC_Block block)
	{
		boolean isProperToolUsed = false;
		MC_World world = this.Server.getWorld(loc.dimension);
		MC_ItemStack toolUsed = player.getItemInHand();

		ArrayList<String> requiredTools = (ArrayList<String>) config.Tools.get("required-tool");

		for (String item : requiredTools)
		{
			if (toolUsed.getFriendlyName().toLowerCase().equals(item.toLowerCase()))
			{
				isProperToolUsed = true;
			}
		}

		MC_Block blockBelow = world.getBlockAt((int) loc.x, (int) loc.y - 1, (int) loc.z);
		MC_Block blockAbove = world.getBlockAt((int) loc.x, (int) loc.y + 1, (int) loc.z);
		int count = 1;

		if (isProperToolUsed && (block.getId() == MC_BlockType.LOG || block.getId() == MC_BlockType.LOG2))
		{
			if (blockBelow.getId() != block.getId())
			{
				// cut the whole tree down
				DisplayDebugMessage("before check");
				if (CheckConnectToLeaves(loc))
				{
					DisplayDebugMessage("check found leaves");
					while (blockAbove.getId() == block.getId())
					{
						world.breakNaturallyAt((int) loc.x, (int) loc.y + count, (int) loc.z, Server.createItemStack(blockAbove.getId(), 1, 0));
						count++;
						blockAbove = world.getBlockAt((int) loc.x, (int) loc.y + count, (int) loc.z);
					}
					DisplayDebugMessage("after check");
				} else
				{
					DisplayDebugMessage("No leaves found... ignore fell");
				}
			} else
			{
				// don't do it man! it's not worth it!
			}
		}
	}

	public boolean CheckConnectToLeaves(MC_Location loc)
	{
		boolean result = false;
		int x1 = (int) loc.x + 1;
		int x2 = (int) loc.x - 1;
		int z1 = (int) loc.z + 1;
		int z2 = (int) loc.z - 1;
		int y = (int) loc.y;

		MC_World world = Server.getWorld(loc.dimension);
		MC_Block blockL, blockR, blockU, blockD;

		for (int count = 0; world.getBlockAt((int) loc.x, (int) loc.y + count, (int) loc.z).getId() != MC_ItemType.AIR; count++)
		{
			blockL = world.getBlockAt((int) loc.x, y + count, z1);
			blockR = world.getBlockAt((int) loc.x, y + count, z2);
			blockU = world.getBlockAt(x1, y + count, (int) loc.z);
			blockD = world.getBlockAt(x2, y + count, (int) loc.z);
			if ((blockL.getId() == MC_ItemType.LEAVES || blockL.getId() == MC_ItemType.LEAVES2) || (blockR.getId() == MC_ItemType.LEAVES || blockL.getId() == MC_ItemType.LEAVES2) || (blockU.getId() == MC_ItemType.LEAVES || blockU.getId() == MC_ItemType.LEAVES2) || (blockD.getId() == MC_ItemType.LEAVES || blockD.getId() == MC_ItemType.LEAVES2))
			{
				DisplayDebugMessage("Leaves found...");
				result = true;
			}
		}
		DisplayDebugMessage("result: " + result);
		return result;
	}
}
