package com.furyan.util;

import java.util.Arrays;
import java.util.List;

import FuryansTreeCapitator.MyPlugin;
import PluginReference.MC_Command;
import PluginReference.MC_Player;

public class TreeCapitatorCommand implements MC_Command
{
	@Override
	public List<String> getAliases()
	{
		return Arrays.asList(new String[]
		{ "lc" });
	}

	@Override
	public String getCommandName()
	{
		return "lorecraft";
	}

	@Override
	public String getHelpLine(MC_Player player)
	{
		return "Adventure lands plugin - Documentation in progress";
	}

	@Override
	public List<String> getTabCompletionList(MC_Player player, String[] args)
	{
		return null;
	}

	@Override
	public void handleCommand(MC_Player player, String[] args)
	{
	}

	@Override
	public boolean hasPermissionToUse(MC_Player player)
	{
		return player.hasPermission("adventurelands.lorestats.ls");
	}
}
