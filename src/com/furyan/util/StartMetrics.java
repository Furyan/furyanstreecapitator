package com.furyan.util;

import java.io.IOException;

import net.pl3x.pl3xlibs.MetricsLite;
import net.pl3x.pl3xlibs.Pl3xLibs;
import net.pl3x.pl3xlibs.scheduler.Pl3xRunnable;
import PluginReference.PluginInfo;

public class StartMetrics extends Pl3xRunnable
{
	private PluginInfo pluginInfo;
	private MetricsLite metrics;

	public StartMetrics(PluginInfo pluginInfo)
	{
		this.pluginInfo = pluginInfo;
	}

	@Override
	public void run()
	{
		Pl3xLibs.getLogger(pluginInfo).info("Starting metrics!");
		try
		{
			metrics = new MetricsLite(pluginInfo);
			metrics.start();
		} catch (IOException e)
		{
			Pl3xLibs.getLogger(pluginInfo).error("Failed to start Metrics: " + e.getMessage());
		}
	}
}