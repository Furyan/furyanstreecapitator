package com.furyan.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import FuryansTreeCapitator.MyPlugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

public class Config
{
	private File _configFile;
	private GsonBuilder _builder;

	@Expose
	public int PluginMajorVersion, PluginMinorVersion;

	@Expose
	public boolean DisplayDebugMessages;

	@Expose
	public HashMap<String, List<String>> Tools;

	public Config()
	{
		_builder = new GsonBuilder();
		_builder.setPrettyPrinting().serializeNulls().excludeFieldsWithoutExposeAnnotation();

		_configFile = new File(MyPlugin.getPluginConfigDirectory(), "config.json");

		PluginMajorVersion = MyPlugin.MAJOR_VERSION;
		PluginMajorVersion = MyPlugin.MINOR_VERSION;
		Tools = new HashMap<String, List<String>>();
	}

	public void LoadConfig()
	{
		Gson gson = _builder.create();

		if (!_configFile.exists())
		{
			GenerateDefaultConfig();

		} else
		{
			System.out.println("=-=-= * Configuration file found! Loading it now...");
			FileInputStream fis;
			try
			{
				fis = new FileInputStream(_configFile);
				InputStreamReader isr = new InputStreamReader(fis);
				BufferedReader br = new BufferedReader(isr);
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null)
				{
					sb.append(line);
				}

				String json = sb.toString();

				Config config = gson.fromJson(json, Config.class);
				SetConfig(config);

			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public void SaveConfig()
	{
		Gson gson = _builder.create();
		String json = gson.toJson(this);

		try
		{
			SaveJsonToFile(_configFile, json);
			System.out.println("=-=-= * Configuration file saved...");
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void GenerateDefaultConfig()
	{
		System.out.println("=-=-= * Configuration file not found! Creating default configuration file...");
		this.DisplayDebugMessages = false;
		ArrayList<String> defaultTools = new ArrayList<String>();
		defaultTools.add("iron axe");
		defaultTools.add("gold axe");
		defaultTools.add("diamond axe");
		this.Tools.put("required-tool", defaultTools);

		Gson gson = _builder.create();
		String json = gson.toJson(this);

		try
		{
			SaveJsonToFile(_configFile, json);
			System.out.println("=-=-= * Default configuration file created...");
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void SaveJsonToFile(File file, String json) throws IOException
	{
		FileOutputStream os;
		os = new FileOutputStream(file);
		os.write(json.getBytes());
		os.close();
	}

	private void SetConfig(Config config)
	{
		if (config.Tools.get("required-tool") == null)
		{
			config.Tools = new HashMap<String, List<String>>();
			ArrayList<String> defaultTools = new ArrayList<String>();
			defaultTools.add("iron axe");
			defaultTools.add("gold axe");
			defaultTools.add("diamond axe");
			this.Tools.put("required-tool", defaultTools);
		}

		this.Tools = config.Tools;
		this.DisplayDebugMessages = config.DisplayDebugMessages;

		SaveConfig();
	}
}